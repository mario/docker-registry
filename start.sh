#!/bin/bash

set -eu

if [[ ! -f /app/data/config.yml ]]; then
    cp /app/pkg/config-example.yml /app/data/config.yml
fi

sed -e "s/realm.*/realm\: ${CLOUDRON_APP_DOMAIN}/g" \
    -e "s/password.*/password\: ${CLOUDRON_REDIS_PASSWORD}/g" \
    -i /app/data/config.yml

sed "26s/.*/  addr\: ${CLOUDRON_REDIS_HOST}\:${CLOUDRON_REDIS_PORT}/g" -i /app/data/config.yml

if [[ ! -f /app/data/htpasswd ]]; then
    cp /app/pkg/htpasswd /app/data/htpasswd
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start the docker registry"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/registry serve /app/data/config.yml


